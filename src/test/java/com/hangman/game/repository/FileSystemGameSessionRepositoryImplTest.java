package com.hangman.game.repository;

import com.hangman.model.GameSession;
import com.hangman.repository.FileSystemGameSessionRepositoryImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import static com.hangman.game.repository.FileSystemGameSessionRepositoryImplTest.SRC_TEST_RESOURCES_TESTDATA;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(properties = {"game.repository.session.basedir=" + SRC_TEST_RESOURCES_TESTDATA})
@ActiveProfiles("test")
public class FileSystemGameSessionRepositoryImplTest {

    public static final String SRC_TEST_RESOURCES_TESTDATA = "src/test/resources/testdata";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemGameSessionRepositoryImplTest.class);

    @Autowired
    private FileSystemGameSessionRepositoryImpl fileSystemGameRepositoryImpl;

    @After
    @Before
    public void cleanTestFileSystemDirectory() throws IOException {
        /**
         * @see http://stackoverflow.com/questions/27644361/how-can-i-throw-checked-exceptions-from-inside-java-8-streams
         */
        Files.list(Paths.get(SRC_TEST_RESOURCES_TESTDATA)).filter(path -> path.getFileName().toString().endsWith(".json")).forEach(
                path -> {
                    try {
                        Files.delete(path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    @Test
    public void saveSingleGameSession() throws Exception {
        boolean saved = fileSystemGameRepositoryImpl.saveGameSessionState(new GameSession("1", "test", 4, 2), false);
        assertTrue(saved);
        Optional<GameSession> gameSession = fileSystemGameRepositoryImpl.getGameSession("1");
        assertTrue(gameSession.isPresent());
        LOGGER.info(gameSession.get().toString());
    }

    /**
     * Chain operations' expectations: create, fail to overwrite, succeed overwriting
     *
     * @throws Exception
     */
    @Test
    public void checkWriteOperationsObeyOverrideRule() {
        boolean savedOne = fileSystemGameRepositoryImpl.saveGameSessionState(new GameSession("anton", "test", 5, 2), false);
        boolean savedTwo = fileSystemGameRepositoryImpl.saveGameSessionState(new GameSession("anton", "test", 5, 2), false);
        assertTrue(savedOne);
        assertFalse(savedTwo); // same game id session file creation should fail
        boolean overriden = fileSystemGameRepositoryImpl.saveGameSessionState(new GameSession("anton", "test2", 5, 2), true);
        assertTrue(overriden); // should override as we set the flag
        assertEquals("test2", fileSystemGameRepositoryImpl.getGameSession("anton").get().getOriginalWord());
        assertEquals(1, fileSystemGameRepositoryImpl.getAllGameSessions().size());
    }

}