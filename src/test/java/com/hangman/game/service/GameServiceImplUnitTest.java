package com.hangman.game.service;

import com.hangman.model.GameSession;
import com.hangman.model.GameView;
import com.hangman.repository.GameSessionRepository;
import com.hangman.service.GameService;
import com.hangman.service.GameServiceImpl;
import com.hangman.status.OperationOutcome;
import com.hangman.status.GameOperationStatus;
import com.hangman.util.GameSessionIdGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static com.hangman.status.GameOperationStatus.FAIL_TO_GENERATE_UNIQUE_IDENTIFIER;
import static com.hangman.status.GameOperationStatus.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceImplUnitTest {

    private static final String DEFAULT_GAME_SESSION_ID = "123";
    private static final String WORD_TO_GUESS = "red";
    private static final char[] CHARS_OF_WORD_TO_GUESS = WORD_TO_GUESS.toCharArray();
    private static final char COVERED_LETTER_MASK = '-';
    public static final String USER_SESSION_ID = "anton";

    @Mock
    private GameSessionIdGenerator gameSessionIdGenerator;
    @Mock
    private GameSessionRepository gameSessionRepository;

    private GameService gameService;

    @Before
    public void setUp() {
        gameService = new GameServiceImpl(gameSessionRepository, gameSessionIdGenerator);
    }

    @Test
    public void addNewGameSession() throws Exception {
        when(gameSessionIdGenerator.generateGameSessionId()).thenReturn(DEFAULT_GAME_SESSION_ID);
        when(gameSessionRepository.saveGameSessionState(any(), eq(false))).thenReturn(true);
        OperationOutcome<GameView> gameView = gameService.addNewGameSession("red", 5, 3);

        verify(gameSessionIdGenerator).generateGameSessionId();
        verify(gameSessionRepository).saveGameSessionState(any(), eq(false));
        assertTrue(gameView.getStatus().equals(SUCCESS));
    }

    @Test
    public void failToAddNewGameSessionAfterThreeClashes() throws Exception {
        when(gameSessionIdGenerator.generateGameSessionId()).thenReturn(DEFAULT_GAME_SESSION_ID);
        when(gameSessionRepository.saveGameSessionState(any(), eq(false))).thenReturn(true);
        OperationOutcome<GameView> firstOpOutcome = gameService.addNewGameSession("red", 5, 3);
        OperationOutcome<GameView> secondOpOutcome = gameService.addNewGameSession("red", 5, 3);
        assertEquals(SUCCESS, firstOpOutcome.getStatus());
        assertEquals(FAIL_TO_GENERATE_UNIQUE_IDENTIFIER, secondOpOutcome.getStatus());
    }


    @Test
    public void failJoinNonExistentGameSession() throws Exception {
        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.empty());
        OperationOutcome<GameView> operationOutcome = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.FAIL_GAME_DOES_NOT_EXIST, operationOutcome.getStatus());
    }

    @Test
    public void failJoinAsGameSessionHasEnded() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, "red", 4, 2);
        gameSession.setWon(true);

        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession));
        OperationOutcome<GameView> operationOutcome = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, "red");
        assertEquals(GameOperationStatus.FAIL_GAME_ALREADY_ENDED, operationOutcome.getStatus());
    }

    @Test
    public void failJoinAsGameSessionIsFull() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, "red", 4, 1);

        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession));
        when(gameSessionRepository.saveGameSessionState(gameSession, true)).thenReturn(true);

        OperationOutcome<GameView> firstPlayerJoinOp = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, firstPlayerJoinOp.getStatus());
        OperationOutcome<GameView> secondPlayerJoinOp = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, "antonTwo");
        assertEquals(GameOperationStatus.FAIL_NUMBER_OF_ALLOWED_PLAYERS_EXCEEDED, secondPlayerJoinOp.getStatus());
    }

    @Test
    public void failJoinAsAlreadyJoined() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID, 4, 2);

        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession), Optional.of(gameSession));
        when(gameSessionRepository.saveGameSessionState(gameSession, true)).thenReturn(true);

        OperationOutcome<GameView> firstPlayerJoinOp = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, firstPlayerJoinOp.getStatus());

        OperationOutcome<GameView> sameFirstPlayerJoinOp = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.FAIL_USER_ALREADY_PLAYS_THIS_GAME, sameFirstPlayerJoinOp.getStatus());
    }

    @Test
    public void failJoinAsSaveToRepositoryFailed() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, "excavation", 4, 2);

        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession));

        OperationOutcome<GameView> firstPlayerJoinOp = gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.FAIL_TO_SAVE_GAME, firstPlayerJoinOp.getStatus());
    }

    @Test
    public void playAndWinGameSinglePlayer() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, WORD_TO_GUESS, 4, 2);
        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession));
        when(gameSessionRepository.saveGameSessionState(gameSession, true)).thenReturn(true);

        assertEquals(GameOperationStatus.SUCCESS, gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID).getStatus());

        // fail guess
        OperationOutcome<GameView> gameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, gameViewOperationOutcome.getStatus());

        // fail guess check
        GameView gameView = gameViewOperationOutcome.getOptionalValue().get();
        assertEquals(1, gameView.getNumberOfMistakes());
        assertTrue(gameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(gameView.isWon());
        assertFalse(gameView.isLost());

        // correct guess
        OperationOutcome<GameView> secondGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession(CHARS_OF_WORD_TO_GUESS[0], DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, gameViewOperationOutcome.getStatus());

        //correct guess check
        GameView secondGameView = secondGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(1, secondGameView.getNumberOfMistakes());
        System.out.println(secondGameView.getCurrentUnveiledLetters());
        assertTrue(secondGameView.getCurrentUnveiledLetters().stream().anyMatch(character -> character == CHARS_OF_WORD_TO_GUESS[0]));
        assertFalse(secondGameView.isWon());
        assertFalse(secondGameView.isLost());

        // correct guess
        OperationOutcome<GameView> thirdGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession(CHARS_OF_WORD_TO_GUESS[1], DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, gameViewOperationOutcome.getStatus());

        //correct guess check
        GameView thirdGameView = thirdGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(1, thirdGameView.getNumberOfMistakes());
        System.out.println(thirdGameView.getCurrentUnveiledLetters());
        assertTrue(thirdGameView.getCurrentUnveiledLetters().stream().anyMatch(character -> character == CHARS_OF_WORD_TO_GUESS[1]));
        assertFalse(thirdGameView.isWon());
        assertFalse(thirdGameView.isLost());

        // correct guess
        OperationOutcome<GameView> fourthGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession(CHARS_OF_WORD_TO_GUESS[2], DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, gameViewOperationOutcome.getStatus());

        //final correct guess check
        GameView fourthGameView = fourthGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(1, fourthGameView.getNumberOfMistakes());
        System.out.println(fourthGameView.getCurrentUnveiledLetters());
        assertTrue(fourthGameView.getCurrentUnveiledLetters().stream().anyMatch(character -> character == CHARS_OF_WORD_TO_GUESS[2]));
        assertTrue(fourthGameView.isWon());
        assertFalse(fourthGameView.isLost());
    }

    @Test
    public void playAndLoseGame() throws Exception {
        GameSession gameSession = new GameSession(DEFAULT_GAME_SESSION_ID, WORD_TO_GUESS, 4, 2);
        when(gameSessionRepository.getGameSession(DEFAULT_GAME_SESSION_ID)).thenReturn(Optional.of(gameSession));
        when(gameSessionRepository.saveGameSessionState(gameSession, true)).thenReturn(true);

        assertEquals(GameOperationStatus.SUCCESS, gameService.joinGameSession(DEFAULT_GAME_SESSION_ID, USER_SESSION_ID).getStatus());

        // fail guess
        OperationOutcome<GameView> gameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, gameViewOperationOutcome.getStatus());

        // fail guess check
        GameView gameView = gameViewOperationOutcome.getOptionalValue().get();
        assertEquals(1, gameView.getNumberOfMistakes());
        assertTrue(gameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(gameView.isWon());
        // fail guess
        OperationOutcome<GameView> secondGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, secondGameViewOperationOutcome.getStatus());

        // fail guess check
        GameView secondGameView = secondGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(2, secondGameView.getNumberOfMistakes());
        assertTrue(secondGameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(secondGameView.isWon());
        assertFalse(secondGameView.isLost());

        // fail guess
        OperationOutcome<GameView> thirdGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, thirdGameViewOperationOutcome.getStatus());

        // fail guess check
        GameView thirdGameView = thirdGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(3, thirdGameView.getNumberOfMistakes());
        assertTrue(thirdGameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(thirdGameView.isWon());
        assertFalse(thirdGameView.isLost());

        // fail guess
        OperationOutcome<GameView> fourthGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, fourthGameViewOperationOutcome.getStatus());

        // fail guess check
        GameView fourthGameView = fourthGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(4, fourthGameView.getNumberOfMistakes());
        assertTrue(fourthGameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(fourthGameView.isWon());
        assertFalse(fourthGameView.isLost());

        // final fail guess
        OperationOutcome<GameView> fifthGameViewOperationOutcome = gameService.guessLetterForGameSessionByUserSession('z', DEFAULT_GAME_SESSION_ID, USER_SESSION_ID);
        assertEquals(GameOperationStatus.SUCCESS, fifthGameViewOperationOutcome.getStatus());

        // final fail guess check
        GameView fifthGameView = fifthGameViewOperationOutcome.getOptionalValue().get();
        assertEquals(5, fifthGameView.getNumberOfMistakes());
        assertTrue(fifthGameView.getCurrentUnveiledLetters().stream().allMatch(character -> character == COVERED_LETTER_MASK));
        assertFalse(fifthGameView.isWon());
        assertTrue(fifthGameView.isLost());
    }
}