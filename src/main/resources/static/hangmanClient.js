$(document).ready(function () {
    getAllGames();
    $('#newGameForm').submit(function (ev) {
        ev.preventDefault();
        saveOneGame(this.elements["word"].value, this.elements["mistakesLimit"].value, this.elements["playersLimit"].value);
    });

    $('#gamesTable').find('tbody').on("click", "button", function (ev) {
        ev.preventDefault();
        joinGame(this.parentNode.parentNode.children[0].innerHTML);
    });

    $('#playGameForm').submit(function (ev) {
        ev.preventDefault();
        guessLetterInGame(currentGameId, this.elements["letter"].value);
    });
});


var getAllGames = function () {
    console.log("getting games!");
    $.getJSON("/api/games", function (data) {

        var gamesTable = $('#gamesTable').find("tbody");
        gamesTable.empty();
        data.forEach(function (gameSession) {
            var tableRow = document.createElement("tr");
            gamesTable.append(tableRow);

            var idTableData = document.createElement("td");
            tableRow.appendChild(idTableData);
            idTableData.innerHTML = gameSession.id;

            var statusData = document.createElement("td");
            tableRow.appendChild(statusData);

            statusData.innerHTML = !gameSession.won && !gameSession.lost ? "active" : gameSession.won ? "finished(WON)" : "finished(HANGOVER)";

            var wordProgressData = document.createElement("td");
            tableRow.appendChild(wordProgressData);

            var word = '';
            gameSession.currentUnveiledLetters.forEach(function (letter) {
                word = word.concat(letter);
            });
            wordProgressData.innerHTML = word;

            var mistakesLimitData = document.createElement("td");
            tableRow.appendChild(mistakesLimitData);
            mistakesLimitData.innerHTML = gameSession.mistakesLimit;

            var numberOfMistakes = document.createElement("td");
            tableRow.appendChild(numberOfMistakes);
            numberOfMistakes.innerHTML = gameSession.numberOfMistakes;

            var numberOfPlayers = document.createElement("td");
            tableRow.appendChild(numberOfPlayers);
            numberOfPlayers.innerHTML = gameSession.numberOfPlayers;

            var playersLimit = document.createElement("td");
            tableRow.appendChild(playersLimit);
            playersLimit.innerHTML = gameSession.playersLimit;

            var action = document.createElement("td");
            tableRow.appendChild(action);

            var actionElement = document.createElement("button");
            actionElement.innerHTML = "Play";
            action.appendChild(actionElement);
        });
    });

};

var saveOneGame = function (word, mistakesLimit, playersLimit) {
    var gameSetup = {};
    gameSetup.word = word;
    gameSetup.mistakesLimit = mistakesLimit;
    gameSetup.playersLimit = playersLimit;

    console.log("saving new game!");
    $.ajax({
        url: '/api/games',
        type: 'PUT',
        contentType: "application/json",
        data: JSON.stringify(gameSetup),
        success: function (data) {
            getAllGames();
            alert('Successfully saved new game!');
        },
        error: function (request, status, error) {
            alert('Fail to save game: ' + error);
        }
    });

};

var joinGame = function (id) {
    console.log("joining new game!");
    $.ajax({
        url: '/api/games/' + id,
        type: 'POST',
        contentType: "application/json",
        success: function (data) {

            if (!data.lost && !data.won) {

                window.currentGameId = id;
                alert('Successfully joined game !');
                getAllGames();

                $("#activeGameContainer").attr("style", "visibility: visible");
                $("#result").attr("style", "visibility: hidden");

                var word = '';
                data.currentUnveiledLetters.forEach(function (letter) {
                    word = word.concat(letter);
                });
                var wordProgressParagraph = $("#word_progress");
                wordProgressParagraph.text("Word Progress: " + word);

                var numberOfPlayers = $("#number_of_players");
                numberOfPlayers.text("Number of Players: " + data.numberOfPlayers);

                var mistakesLeftParagraph = $("#mistakes_left");

                if (data.mistakesLimit <= -1) {
                    mistakesLeftParagraph.text("The executor went for pint, you may practice infinitely. ");
                }
                else {
                    var mistakesLeft = data.mistakesLimit - data.numberOfMistakes;
                    if (mistakesLeft === 0) {
                        alert("This is final attempt before you are hanged!")
                    }
                    mistakesLeftParagraph.text("Mistakes left until executor hangs you: " + mistakesLeft);
                }
            } else {
                alert("Game has finished!")
            }
        },
        error: function (request, status, error) {
            window.currentGameId = null;
            alert('Fail to join game:' + request.responseJSON);
        }
    });

};


var guessLetterInGame = function (id, letter) {
    console.log("joining new game!");
    $.ajax({
        url: '/api/games/' + id + "/" + letter,
        type: 'POST',
        contentType: "application/json",
        success: function (data) {
            var mistakesLeft = data.mistakesLimit - data.numberOfMistakes;

            if (data.correctGuess) {
                alert('That is correct guess!');
            }

            getAllGames();

            if (!data.lost && !data.won) {
                var word = '';
                data.currentUnveiledLetters.forEach(function (letter) {
                    word = word.concat(letter);
                });
                var wordProgressParagraph = $("#word_progress");
                wordProgressParagraph.text("Word Progress: " + word);

                var numberOfPlayers = $("#number_of_players");
                numberOfPlayers.text("Number of Players: " + data.numberOfPlayers);

                var mistakesLeftParagraph = $("#mistakes_left");

                if (data.mistakesLimit <= -1) {
                    mistakesLeftParagraph.text("The executor went for a pint, you may practice infinitely.");
                }
                else {

                    if (mistakesLeft === 0) {
                        alert("This is final attempt before you are hanged!")
                    }
                    mistakesLeftParagraph.text("Mistakes left until executor hangs you: " + mistakesLeft);
                }

            }
            else if (data.lost) {
                var gameContainer = $("#activeGameContainer");
                gameContainer.attr("style", "visibility: hidden");
                var img = $('<img id="dynamic">');
                img.attr('src', "favicon.ico");
                img.attr('width', "500");
                img.attr('height', "500");

                $("#result").empty();
                $("#result").attr("style", "visibility: visible");
                $("#result").append(img);
            }
            else if (data.won) {
                var gameContainer = $("#activeGameContainer");
                gameContainer.attr("style", "visibility: hidden");

                var img = $('<img id="dynamic">');
                img.attr('src', "victory.jpg");
                img.attr('width', "500");
                img.attr('height', "500");
                $("#result").empty();
                $("#result").attr("style", "visibility: visible");
                $("#result").append(img);
            }

        },
        error: function (request, status, error) {
            alert("Fail to guess letter: " + letter +
                " for game: " + id +
                " with reason: " + request.responseJSON);
        }
    });

};

