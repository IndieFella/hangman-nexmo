package com.hangman.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hangman.status.GuessOutcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// No synchronisation on this class as the state factually shared through the .json files identifiers, operations to these are synchronised
// on file names
public class GameSession {
    private final static Logger LOGGER = LoggerFactory.getLogger(GameSession.class);
    private String id;
    private List<LetterState> lettersState;
    private int sessionsLimit;
    private List<String> sessions = new ArrayList<>();
    private int mistakesLimit;
    private int numberOfMistakes = 0;
    private boolean lost;
    private boolean won;

    public GameSession() {
    }

    public GameSession(String id, String secretWord, int mistakesLimit, int sessionsLimit) {
        this.id = id;
        this.lettersState = initializeLettersStateForWord(secretWord);
        this.mistakesLimit = mistakesLimit;
        this.sessionsLimit = sessionsLimit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GameView guessCharacter(char character) {
        boolean atLeastOneLetterIsUnveiled = false;
        if (gameIsActive()) {
            // can't use anyMatch as it does not check all duplicate letters, first match and return
            long numberOfCorrectMatches = lettersState.stream().map(letterState -> letterState.guess(character) == GuessOutcome.CORRECT_GUESS).filter(Boolean::booleanValue).count();

            if (numberOfCorrectMatches > 0) {
                atLeastOneLetterIsUnveiled = true;
            }
            saveProgressState(atLeastOneLetterIsUnveiled);
        }

        return getGameView(atLeastOneLetterIsUnveiled);
    }

    @JsonIgnore
    public GameView getGameView() {
        return getGameView(null);
    }

    @JsonIgnore
    public GameView getGameView(Boolean correctGuess) {
        return new GameView(getId(), getKnownLettersWithCorrespondingPositions(lettersState), numberOfMistakes, mistakesLimit, sessions.size(), sessionsLimit, correctGuess, won, lost);
    }

    private void saveProgressState(boolean atLeastOneLetterIsUnveiled) {
        if (!atLeastOneLetterIsUnveiled) {
            numberOfMistakes++;
            saveDefeatStateIfLost();
        } else {
            saveWinStateIfWon();
        }
    }

    public List<String> getSessions() {
        return sessions;
    }

    public void setSessions(List<String> sessions) {
        this.sessions = sessions;
    }

    public void registerSession(String session) {
        sessions.add(session);
    }

    @JsonIgnore
    private boolean gameIsActive() {
        return !lost || !won;
    }

    public List<LetterState> getLettersState() {
        return lettersState;
    }

    public void setLettersState(List<LetterState> lettersState) {
        this.lettersState = lettersState;
    }

    @JsonIgnore
    public String getOriginalWord() {
        StringBuffer stringBuffer = new StringBuffer();

        lettersState.forEach(
                letterState -> stringBuffer.append(letterState.getLetter()));
        return stringBuffer.toString();
    }

    public int getMistakesLimit() {
        return mistakesLimit;
    }

    public void setMistakesLimit(int mistakesLimit) {
        this.mistakesLimit = mistakesLimit;
    }

    public int getNumberOfMistakes() {
        return numberOfMistakes;
    }

    public void setNumberOfMistakes(int numberOfMistakes) {
        this.numberOfMistakes = numberOfMistakes;
    }

    public boolean isLost() {
        return lost;
    }

    public void setLost(boolean lost) {
        this.lost = lost;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public int getSessionsLimit() {
        return sessionsLimit;
    }

    public void setSessionsLimit(int sessionsLimit) {
        this.sessionsLimit = sessionsLimit;
    }

    private void saveWinStateIfWon() {
        boolean allLettersUnveiled = lettersState.stream().allMatch(LetterState::isUnveiled);

        if (allLettersUnveiled) {
            won = true;
        }
    }

    private void saveDefeatStateIfLost() {
        if (mistakesLimit > -1 && mistakesLimit - numberOfMistakes < 0) {
            LOGGER.info("The game with id: {} has been lost!", id);
            lost = true;
        }
    }

    private List<LetterState> initializeLettersStateForWord(String secretWord) {
        List<LetterState> letterStates = new ArrayList<>();

        char[] chars = secretWord.toCharArray();

        for (char letter : chars) {
            letterStates.add(new LetterState(letter));
        }

        return letterStates;
    }

    private List<Character> getKnownLettersWithCorrespondingPositions(List<LetterState> letterStates) {
        return letterStates.stream().map(LetterState::getUnveiledLetter).collect(Collectors.toList());
    }

    @JsonIgnore
    public boolean isGameEnded() {
        return this.lost || this.won;
    }

    @Override
    public String toString() {
        return "GameSession{" +
                "id='" + id + '\'' +
                ", lettersState=" + lettersState +
                ", sessionsLimit=" + sessionsLimit +
                ", sessions=" + sessions +
                ", mistakesLimit=" + mistakesLimit +
                ", numberOfMistakes=" + numberOfMistakes +
                ", lost=" + lost +
                ", won=" + won +
                '}';
    }
}
