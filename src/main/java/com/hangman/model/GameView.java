package com.hangman.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Describes the current status of the game
 */
public class GameView {
    private final String id;
    private final List<Character> currentUnveiledLetters;
    private final int numberOfMistakes;
    private final int mistakesLimit;
    private final int numberOfPlayers;
    private final int playersLimit;
    private final Boolean correctGuess;
    private final boolean won;
    private final boolean lost;

    public GameView(
                    @JsonProperty("id") String id,
                    @JsonProperty("currentUnveiledLetters") List<Character> currentUnveiledLetters,
                    @JsonProperty("numberOfMistakes") int numberOfMistakes,
                    @JsonProperty("mistakesLimit") int mistakesLimit,
                    @JsonProperty("numberOfPlayers") int numberOfPlayers,
                    @JsonProperty("playersLimit") int playersLimit,
                    @JsonProperty("correctGuess") Boolean correctGuess,
                    @JsonProperty("won") boolean won,
                    @JsonProperty("lost") boolean lost) {
        this.id = id;
        this.currentUnveiledLetters = currentUnveiledLetters;
        this.numberOfMistakes = numberOfMistakes;
        this.mistakesLimit = mistakesLimit;
        this.numberOfPlayers = numberOfPlayers;
        this.playersLimit = playersLimit;
        this.correctGuess = correctGuess;
        this.won = won;
        this.lost = lost;
    }

    public List<Character> getCurrentUnveiledLetters() {
        return currentUnveiledLetters;
    }

    public int getNumberOfMistakes() {
        return numberOfMistakes;
    }

    public int getMistakesLimit() {
        return mistakesLimit;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public int getPlayersLimit() {
        return playersLimit;
    }

    public Boolean getCorrectGuess() {
        return correctGuess;
    }

    public String getId() {
        return id;
    }

    public boolean isWon() {
        return won;
    }

    public boolean isLost() {
        return lost;
    }
}
