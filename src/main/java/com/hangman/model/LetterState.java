package com.hangman.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hangman.status.GuessOutcome;

public class LetterState {
    private char letter;
    private boolean isUnveiled;

    public LetterState() {
    }

    public LetterState(char letter) {
        this.letter = letter;
    }

    public GuessOutcome guess(char unconfirmedLetter) {

        if (letter != unconfirmedLetter) {
            return GuessOutcome.INCORRECT_GUESS;
        } else if (!isUnveiled) {
            isUnveiled = true;
            return GuessOutcome.CORRECT_GUESS;
        }
        return GuessOutcome.SOMEBODY_GUESSED_BEFORE;
    }

    public boolean isUnveiled() {
        return isUnveiled;
    }

    @JsonIgnore
    public char getUnveiledLetter(){
        if(isUnveiled()){
            return letter;
        }
        return '-';
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public void setUnveiled(boolean unveiled) {
        isUnveiled = unveiled;
    }

    @Override
    public String toString() {
        return "LetterState{" +
                "letter=" + letter +
                ", isUnveiled=" + isUnveiled +
                '}';
    }
}
