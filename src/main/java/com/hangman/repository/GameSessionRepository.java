package com.hangman.repository;

import com.hangman.model.GameSession;

import java.util.List;
import java.util.Optional;

public interface GameSessionRepository {
    Optional<GameSession> getGameSession(String uuid);
    boolean saveGameSessionState(GameSession gameSession, boolean override);
    List<GameSession> getAllGameSessions();
}
