package com.hangman.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hangman.model.GameSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class FileSystemGameSessionRepositoryImpl implements GameSessionRepository {
    private static final String FILE_FORMAT_JSON = ".json";

    private ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemGameSessionRepositoryImpl.class);

    @PostConstruct
    public void checkFileSystem() throws IOException {
        if (!Files.exists(Paths.get(BASE_DIR))) {
            throw new FileNotFoundException("Please create base directory in your file system: " + BASE_DIR + " (optionally change application.yml base directory");
        }
    }

    @Value("${game.repository.session.basedir}")
    private String BASE_DIR;

    @Override
    public Optional<GameSession> getGameSession(final String UUID) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(constructPath(UUID))) {
            return Optional.of(mapper.readValue(bufferedReader, GameSession.class));
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public boolean saveGameSessionState(GameSession gameSession, boolean override) {
        StandardOpenOption standardOpenOption = override ? StandardOpenOption.WRITE : StandardOpenOption.CREATE_NEW;

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(constructPath(gameSession.getId()), standardOpenOption)) {
            mapper.writeValue(bufferedWriter, gameSession);
            return true;
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return false;
        }
    }

    @Override
    public List<GameSession> getAllGameSessions() {
        try {
            return Files.list(Paths.get(BASE_DIR)).filter(path -> path.getFileName().toString().endsWith(FILE_FORMAT_JSON))
                    .map(path -> {
                        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
                            return mapper.readValue(bufferedReader, GameSession.class);
                        } catch (IOException ioException) {
                            return null;
                        }
                    }).filter(Objects::nonNull).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private Path constructPath(String randomUUID) throws IOException {
        return Paths.get(BASE_DIR + "/" + randomUUID + FILE_FORMAT_JSON);
    }
}
