package com.hangman.service;

import com.hangman.model.GameView;
import com.hangman.status.OperationOutcome;

import java.util.List;

public interface GameService {
    OperationOutcome<GameView> addNewGameSession(String word, Integer mistakesLimit, Integer numberOfPlayers);

    OperationOutcome<GameView> getGameView(String UUID);

    OperationOutcome<GameView> joinGameSession(String UUID, String userSessionId);
    OperationOutcome<GameView> guessLetterForGameSessionByUserSession(char letter, String UUID, String userId);

    List<GameView> getAllActiveGames();
}
