package com.hangman.service;

import com.hangman.model.GameView;
import com.hangman.model.GameSession;
import com.hangman.repository.GameSessionRepository;
import com.hangman.status.OperationOutcome;
import com.hangman.status.GameOperationStatus;
import com.hangman.util.GameSessionIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {

    private static final int TWO_PLAYERS = 2;
    private final GameSessionRepository gameSessionRepository;
    private final GameSessionIdGenerator gameSessionIdGenerator;

    private final Map<String, Lock> gameIdsToLocks = new ConcurrentHashMap<>();
    private static Logger LOGGER = LoggerFactory.getLogger(GameServiceImpl.class);

    @Value("${game.rule.mistake.limit}")
    private Integer DEFAULT_MISTAKES_LIMIT;

    @Autowired
    public GameServiceImpl(GameSessionRepository gameSessionRepository, GameSessionIdGenerator gameSessionIdGenerator) {
        this.gameSessionRepository = gameSessionRepository;
        this.gameSessionIdGenerator = gameSessionIdGenerator;
    }

    /**
     * @param word secret word to guess
     * @param mistakesLimit 0 = sudden death, one mistake = game over; -1 = practice play until entire word is uncovered
     * @return OperationOutcome with GameView if success
     */
    @Override
    public OperationOutcome<GameView> addNewGameSession(String word, Integer mistakesLimit, Integer playersLimit) {
        if (mistakesLimit == null) {
            mistakesLimit = DEFAULT_MISTAKES_LIMIT;
        }

        if (playersLimit == null) {
            playersLimit = TWO_PLAYERS;
        }

        Optional<String> generatedGameSessionId = generateGameSessionId(3);

        if (!generatedGameSessionId.isPresent()) {
            return new OperationOutcome<>(GameOperationStatus.FAIL_TO_GENERATE_UNIQUE_IDENTIFIER);
        }

        GameSession gameSession = new GameSession(generatedGameSessionId.get(), word, mistakesLimit, playersLimit);
        gameSession.setId(generatedGameSessionId.get());

        boolean saved = gameSessionRepository.saveGameSessionState(gameSession, false);

        if (!saved) {
            return new OperationOutcome<>(GameOperationStatus.FAIL_TO_SAVE_GAME);
        }
        return new OperationOutcome<>(gameSession.getGameView());
    }

    @Override
    public OperationOutcome<GameView> getGameView(String gameId) {
        Lock gameIdLock = getGameIdLock(gameId);
        gameIdLock.lock();

        try {
            Optional<GameSession> gameSession = gameSessionRepository.getGameSession(gameId);
            if (gameSession.isPresent()) {
                return new OperationOutcome<>(gameSession.get().getGameView());
            }
            return new OperationOutcome<>(GameOperationStatus.FAIL_GAME_DOES_NOT_EXIST);
        } finally {
            gameIdLock.unlock();
        }
    }

    @Override
    public OperationOutcome<GameView> joinGameSession(String gameId, String userSessionId) {
        Lock gameIdLock = getGameIdLock(gameId);
        gameIdLock.lock();

        try {
            Optional<GameSession> gameSession = gameSessionRepository.getGameSession(gameId);
            if (gameSession.isPresent()) {
                GameSession existingGameSession = gameSession.get();

                if (existingGameSession.isGameEnded()) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_GAME_ALREADY_ENDED, existingGameSession.getGameView());
                } else if (existingGameSession.getSessions().stream().anyMatch(existingUserSessionId -> existingUserSessionId.equals(userSessionId))) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_USER_ALREADY_PLAYS_THIS_GAME, existingGameSession.getGameView());
                } else if (existingGameSession.getSessions().size() >= existingGameSession.getSessionsLimit()) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_NUMBER_OF_ALLOWED_PLAYERS_EXCEEDED);
                }

                existingGameSession.registerSession(userSessionId);
                boolean saved = gameSessionRepository.saveGameSessionState(existingGameSession, true);
                if (!saved) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_TO_SAVE_GAME);
                }

                return new OperationOutcome<>(GameOperationStatus.SUCCESS, existingGameSession.getGameView());
            }
            return new OperationOutcome<>(GameOperationStatus.FAIL_GAME_DOES_NOT_EXIST);
        } finally {
            gameIdLock.unlock();
        }
    }

    @Override
    public OperationOutcome<GameView> guessLetterForGameSessionByUserSession(char letter, String gameId, String
            userId) {
        Lock gameIdLock = getGameIdLock(gameId);
        gameIdLock.lock();

        try {
            Optional<GameSession> gameSession = gameSessionRepository.getGameSession(gameId);

            if (gameSession.isPresent()) {
                GameSession existingGameSession = gameSession.get();
                if (existingGameSession.getSessions().stream().noneMatch(playingSession -> playingSession.equals(userId))) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_UNAUTHORISED);
                }
                if (existingGameSession.isGameEnded()) {
                    return new OperationOutcome<>(GameOperationStatus.FAIL_GAME_ALREADY_ENDED, existingGameSession.getGameView());
                }

                GameView gameView = existingGameSession.guessCharacter(letter);

                boolean saved = gameSessionRepository.saveGameSessionState(existingGameSession, true);

                if (saved) {
                    return new OperationOutcome<>(GameOperationStatus.SUCCESS, gameView);
                }

                return new OperationOutcome<>(GameOperationStatus.FAIL_TO_SAVE_GAME);
            }
            return new OperationOutcome<>(GameOperationStatus.FAIL_GAME_DOES_NOT_EXIST);
        } finally {
            gameIdLock.unlock();
        }
    }

    // TODO cache to few secs, need EHCache for TTL
    @Override
    public List<GameView> getAllActiveGames() {
        return gameSessionRepository.getAllGameSessions().stream().map(GameSession::getGameView).collect(Collectors.toList());
    }

    private Lock getGameIdLock(String gameId) {
        if (gameIdsToLocks.get(gameId) == null) {
            LOGGER.warn("Could not find gameId of the game session, this could be due to server restart or game not existing.");
            gameIdsToLocks.putIfAbsent(gameId, new ReentrantLock());
        }

        return gameIdsToLocks.get(gameId);
    }

    private Optional<String> generateGameSessionId(int numberOfTries) {
        String gameSessionIdentifier = gameSessionIdGenerator.generateGameSessionId();
        if (gameIdsToLocks.putIfAbsent(gameSessionIdentifier, new ReentrantLock(true)) != null) {
            LOGGER.debug("Generated colliding game identifier: {}", gameSessionIdentifier);
            if (numberOfTries != 0) {
                LOGGER.debug("Trying to generate new...");
                return generateGameSessionId(--numberOfTries);
            } else {
                LOGGER.error("Exceeded number of tries to generate game id, please check the entropy of your game generator on increase identifier length.");
                return Optional.empty();
            }
        }
        return Optional.of(gameSessionIdentifier);
    }
}
