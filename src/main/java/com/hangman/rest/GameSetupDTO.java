package com.hangman.rest;

public class GameSetupDTO {
    private String word;
    private Integer playersLimit;
    private Integer mistakesLimit;

    public GameSetupDTO() {
    }

    public String getWord() {
        return word;
    }

    public Integer getPlayersLimit() {
        return playersLimit;
    }

    public Integer getMistakesLimit() {
        return mistakesLimit;
    }


}
