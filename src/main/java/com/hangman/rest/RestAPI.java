package com.hangman.rest;

import com.hangman.model.GameView;
import com.hangman.service.GameService;
import com.hangman.status.OperationOutcome;
import com.hangman.status.GameOperationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("api")
public class RestAPI {

    @Autowired
    private GameService gameService;

    @GetMapping("/games")
    public ResponseEntity<List<GameView>> getAllGames() {
        return ResponseEntity.ok(gameService.getAllActiveGames());
    }

    @PutMapping("/games")
    public ResponseEntity createNewGame(@RequestBody GameSetupDTO gameSetupDTO) {
        OperationOutcome<GameView> createNewGameOpOutcome = gameService.addNewGameSession(gameSetupDTO.getWord(), gameSetupDTO.getMistakesLimit(), gameSetupDTO.getPlayersLimit());
        if (createNewGameOpOutcome.getStatus().equals(GameOperationStatus.SUCCESS)) {
            return ResponseEntity.ok(createNewGameOpOutcome.getOptionalValue().get());
        }
        else {
            return ResponseEntity.badRequest().body(createNewGameOpOutcome.getStatus());
        }
    }

    @PostMapping("/games/{gameSessionId}")
    public ResponseEntity joinGame(@PathVariable String gameSessionId, HttpSession httpSession) {
        OperationOutcome<GameView> createNewGameOutcome = gameService.joinGameSession(gameSessionId, httpSession.getId());
        if (createNewGameOutcome.getStatus().equals(GameOperationStatus.SUCCESS) || createNewGameOutcome.getOptionalValue().isPresent()) {
            return ResponseEntity.ok(createNewGameOutcome.getOptionalValue().get());
        }
        else {
            return ResponseEntity.badRequest().body(createNewGameOutcome.getStatus());
        }
    }

    @PostMapping("/games/{gameSessionId}/{letterToGuess}")
    public ResponseEntity guessLetterForGameSession(@PathVariable String gameSessionId, @PathVariable char letterToGuess, HttpSession httpSession) {
        OperationOutcome<GameView> joinNewGameOutcome = gameService.guessLetterForGameSessionByUserSession(letterToGuess, gameSessionId, httpSession.getId());
        if (joinNewGameOutcome.getStatus().equals(GameOperationStatus.SUCCESS)) {
            return ResponseEntity.ok(joinNewGameOutcome.getOptionalValue().get());
        }
        else {
            return ResponseEntity.badRequest().body(joinNewGameOutcome.getStatus());
        }
    }


}
