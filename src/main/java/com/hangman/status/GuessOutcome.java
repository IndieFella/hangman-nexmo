package com.hangman.status;

public enum GuessOutcome {
    SOMEBODY_GUESSED_BEFORE,
    CORRECT_GUESS,
    INCORRECT_GUESS
}
