package com.hangman.status;

import java.util.Optional;

/**
 * Stores value and a status explaining operation outcome. If constructed with value then the status should correspond to
 * 'SUCCESS'. If constructed with just status then the operation outcome is negative. Caller may either check the status,
 * or check the optional. This class is convenience measure for testing and caller notification about exact cause, without
 * exception throws.
 *
 * @param <V> value accompanying successful operation outcome
 */
public class OperationOutcome<V> {

    private GameOperationStatus status;
    private V value;

    public OperationOutcome(GameOperationStatus status) {
        this.status = status;
    }

    public OperationOutcome(GameOperationStatus status, V value) {
        if(status.equals(GameOperationStatus.SUCCESS) && value == null){
            throw new IllegalStateException("Success state and no value is illegal and meaningless, if you need this use case" +
                    "do return the enum directly or use normal return types.");
        }
        this.status = status;
        this.value = value;
    }

    public OperationOutcome(V value) {
        status = GameOperationStatus.SUCCESS;
        this.value = value;
    }

    public GameOperationStatus getStatus() {
        return status;
    }

    public Optional<V> getOptionalValue() {
        return Optional.ofNullable(value);
    }
}
