package com.hangman.util;

public interface GameSessionIdGenerator {

    String generateGameSessionId();
}
