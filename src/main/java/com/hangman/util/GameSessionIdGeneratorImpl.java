package com.hangman.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GameSessionIdGeneratorImpl implements GameSessionIdGenerator {
    @Override
    public String generateGameSessionId() {
        return UUID.randomUUID().toString();
    }
}
