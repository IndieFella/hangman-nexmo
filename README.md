# Production Run #

1. Locate: **/src/main/resources/application.yml**
2. Change **server:port:** if you are running something on 8080, or make the port available.
3. Change **game:repository:session:basedir:** to a directory of your choice where game sessions will be saved.
4. Create the directory you just specified.
5. **~/Develop/Projects/hangman $ ./gradlew clean build** (or BAT version for DOS)
6. **java -jar build/libs/hangman-0.0.1-SNAPSHOT.jar** OR **~/Develop/Projects/hangman $ ./gradlew bootRun** as a goal, if you are using IntelliJ then you can simply open **build.gradle** and IJ should recognise the main starter class along with Spring Boot.

Please contact me if there are issues.

# About #
* Uses file system to store game sessions
* The logic is designed to allow concurrent reads/writes to different game sessions(one file represents one game session)
* A lock is obtained for filename; locks storage is restart tolerant(simply repopulates itself on read/write)
* Such approach allows us to avoid synchronisation entirely within our model objects, only ConcurrentMap for game session locks and locks on filename r/w.
* No two or more players will be allowed to operate on same game session simultaneously, we must ensure that player who reads/guesses/writes sees the very last game state.
* Various game rules are respected:FAIL_TO_SAVE_GAME, FAIL_GAME_ALREADY_ENDED, FAIL_NUMBER_OF_ALLOWED_PLAYERS_EXCEEDED, FAIL_UNAUTHORISED, FAIL_GAME_DOES_NOT_EXIST,
FAIL_TO_GENERATE_UNIQUE_IDENTIFIER, FAIL_USER_ALREADY_PLAYS_THIS_GAME
* Application is built using Spring Boot, under the hood it uses embedded Tomcat, Spring MVC, Spring Core.

# Considerations #
* You can press 'Play' several times on the same game and it will recognise you as existing player. 'Play' acts as both: "join new session"/"rejoin the session if I joined before". You cannot play game which you haven't joined.
* **BIG NOTE ON GOOGLE CHROME** -> this browser is my favourite due to handling of **localhost**, as it loves to disrespect the timeout set on cookies and other attributes specifically on **localhost**. It does behave correctly with proper domain names(didn't check IP though). Please use Firefox or add custom mapping in /etc/hosts. The game would work properly if you don't do this, but the cookie will be lost on chrome restart for localhost domain so the server will simply not recognise you as existing player and treat you as new player(http session based implementation). In case where player limit was reached you would not be able to rejoin the session as you will have different session after Chrome restart. 
* The session persistence is entirely based on Spring Boot provision inside TomcatEmbeddedServletContainerFactory activated by: **server:session:persistent: true** ,
as there were no restrictions on frameworks this is the only feature which I got for granted and which matches the spec.
* The UI is built quickly with JQUERY accessing REST API, no backend generated pages, clear separation. index.html is default page served by Spring MVC when no server page frameworks is configured.


# Future Improvements #
* The testing covers full single threaded testing, for multithreaded test I was short on time but intended to use CallableFuture to see how easy is Java 8 construct.
* Cache with short timeout could be introduced to avoid fetching all games for all players. Or alternatively allow only privileged users to see the game sessions table
* Testing should use virtual filesystem, I tried some google library of 2013 which didn't work at all, but Java 8 NIO files api should have something as well
* @JsonIgnore in GameSession is a little bit annoying - could extract the logic in some util class, also many redundant getters for the same class and just for serialiazation which could be replaced with constructor fields annotations, so we don't need to expose fields we never use.

# Logical Future Improvements #
* When creating game the creator has no distinct role, thus can join and play the game -> creator should not be able to guess her own game(maybe in testing mode only)
* When creating a game, hint words could be provided, so players have some help as at the moment game is completely random guessing. Ideally would be fun to have chat with game creator where players could ask some questions to have hints.